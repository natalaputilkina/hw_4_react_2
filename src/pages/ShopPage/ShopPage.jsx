import { useOutletContext } from 'react-router-dom';
import "./ShopPage.scss"
import ProductsList from '../../components/ProductsList/ProductsList';





function ShopPage(){

   

    const { products, cart, favorite } = useOutletContext();

     return (
        <>
            <h1 className='title'>Shop page</h1>
            <div className="shop--page">
                <ProductsList
                    className="shop--page"
                    products={products}                    
                    favorite={favorite}
                    cart={cart}
                />
            </div> 
        </>   
                  

    )
    

}
export default ShopPage