import './Layout.scss'
import cartIcon from "../src/assets/cart.svg"
import starFavoritIcon from "../src/assets/star-favorit.svg"
import { useEffect } from "react"
import { Outlet, Link } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import { fetchProducts } from './productsSlice';



function Layout(){

    const dispatch = useDispatch();
    const products = useSelector((state) => state.products.products);
    const cart = useSelector((state) => state.shop.cart);
    const favorite = useSelector((state) => state.shop.favorite);

    
    useEffect(() => {
        dispatch(fetchProducts());
      }, [dispatch]);

    

    return (
        <>
        <header>
            <div className="header-shop">

                <h1 className="header-title">Mobile-shop</h1>

                <div className='header-menu'>

                    <Link className="header-link" to="/">Shop</Link>
                    <Link className="header-link" to="/wishlist">Wish-list</Link>
                    <Link className="header-link" to="/cart">Cart</Link>

                </div>              

                <div className='header-contener'>

                    <div className="header-favorites">
                        <img className='favorites-img' src={starFavoritIcon} alt="favorites"/>
                        <span className='favorites-count'>{favorite.length}</span>
                    </div>
                    <div className="header-cart">
                        <img className='cart-img' src={cartIcon} alt="cart"/>
                        <span className='cart-count'>{cart.length}</span>
                    </div>
                   
                    
                </div>     

            </div>

              
            
            
        </header>

        <Outlet context={{products, cart, favorite}}/>
       

        
        </>
    )
}

export default Layout