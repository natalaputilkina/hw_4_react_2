import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  cart: [],
  favorite: [],
  isModalOpen: false,
};

const shopSlice = createSlice({
  name: 'shop',
  initialState,
  reducers: {
    addToCart(state, action) {
      state.cart.push(action.payload);
      localStorage.setItem('cart', JSON.stringify(state.cart));
    },
    removeFromCart(state, action) {
      state.cart = state.cart.filter(item => item.id !== action.payload.id);
      localStorage.setItem('cart', JSON.stringify(state.cart));
    },
    
    addToFavorite(state, action) {
      const { favorite } = state;
      const { payload } = action;
      const isProductInFavorite = favorite.some(item => item.id === payload.id);
      if (!isProductInFavorite) {
        state.favorite.push(payload);
        localStorage.setItem('favorite', JSON.stringify(state.favorite));
      }
    },
    removeFromFavorite(state, action) {
      const { favorite } = state;
      const { payload } = action;
      state.favorite = favorite.filter(item => item.id !== payload.id);
      localStorage.setItem('favorite', JSON.stringify(state.favorite));
    },

    initializeCart(state, action) {
      state.cart = action.payload;
    },
    initializeFavorite(state, action) {
      state.favorite = action.payload;
    }
  },
});

export const {addToCart, removeFromCart, addToFavorite, removeFromFavorite,
  initializeCart, initializeFavorite} = shopSlice.actions;
export default shopSlice.reducer;
