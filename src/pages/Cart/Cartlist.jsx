import { useOutletContext } from 'react-router-dom';
import './Cartlist.scss'
import ProductsList from '../../components/ProductsList/ProductsList';
import { useSelector } from 'react-redux';






function CartList(){

    const cart = useSelector((state) => state.shop.cart);
    const { products, favorite } = useOutletContext();

    if (cart.length === 0){
        return (

            <>
                <h1>Cart</h1>
                <h2>Ваш кошик порожній, але можете його поповнити</h2>

            </>
        
        )
    }

    const cartProducts = products.filter((product) => cart.some((cartItem) => cartItem.id === product.id));

    console.log(cartProducts)


    return (
        
            <>
                <h1 className='title'>Cart</h1>
                <ProductsList
                products={cartProducts}                
                favorite={favorite}
                cart={cart}
                 />
            </>
        
    )

        
    
}

export default CartList
