import './ProductsList.scss'
import ProductCard from '../ProductCard/ProductCard';
import { useSelector } from 'react-redux';


const ProductsList = ({ products}) => {

    const cart = useSelector((state) => state.shop.cart);
    const favorite = useSelector((state) => state.shop.favorite);

    return (
        <div className="products-list">
            {products.map((product) => (
                <ProductCard
                    key={product.id}
                    product={product}                    
                    isFavorite={favorite.some((item) => item.id === product.id)}
                    isInCart={cart.some((item) => item.id === product.id)}
                />
            ))}
        </div>
    );
};

export default ProductsList;